package com.avenuecode.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.avenuecode.products.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
}
