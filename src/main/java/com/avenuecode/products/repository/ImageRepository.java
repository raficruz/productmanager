package com.avenuecode.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.avenuecode.products.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long>{
}
