package com.avenuecode.products.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.avenuecode.products.model.Image;
import com.avenuecode.products.repository.ImageRepository;


@Service
public class ImageService{

	public static String UPLOAD_ROOT = "upload-dir";
	
	private final ImageRepository repository;
	private final ResourceLoader resourceLoader;
	
	@Autowired
	public ImageService(ImageRepository repository, ResourceLoader resourceLoader) {
		this.repository = repository;
		this.resourceLoader = resourceLoader;
	}
	
	public Resource findOneImage(String fileName) {
		return resourceLoader.getResource("file:" + UPLOAD_ROOT + "/" + fileName);
	}
	
	public void createImage(MultipartFile file) throws IOException {
		if(!file.isEmpty()) {
			Files.copy(file.getInputStream(), Paths.get(UPLOAD_ROOT, file.getOriginalFilename()));
			repository.saveAndFlush(new Image(file.getContentType()));
			Files.deleteIfExists(Paths.get(UPLOAD_ROOT, file.getOriginalFilename()));
		}
	}
	
	public void deleteImage(Long id) {
		final Image byId = repository.findOne(id);
		repository.delete(byId);
	}
}