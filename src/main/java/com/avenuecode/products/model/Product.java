package com.avenuecode.products.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
@SequenceGenerator(name = "prod_sequence", sequenceName = "product_sequence", initialValue = 1, allocationSize = 1)
public class Product implements Serializable {

	private static final long serialVersionUID = -7303097629416851977L;

	@Id
	@Column(name = "prod_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prod_sequence")
	private Long id;

	@Column(name = "prod_name")
	private String name;

	@Column(name = "prod_description")
	private String description;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "prod_parent_id")
	private Product parent;

	@OneToMany(mappedBy = "parent")
	private List<Product> children;

	@OneToMany(mappedBy = "id", targetEntity = Image.class, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	private List<Image> images;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	public List<Product> getChildren() {
		return children;
	}

	public void setChildren(List<Product> children) {
		this.children = children;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

}