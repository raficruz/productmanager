package com.avenuecode.products.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT_IMAGE")
@SequenceGenerator(name = "img_sequence", sequenceName = "product_image_sequence", initialValue = 1, allocationSize = 1)
public class Image implements Serializable {

	public Image() {
		super();
	}
	
	public Image(String type) {
		this();
		this.setType(type);
	}
	
	private static final long serialVersionUID = 2913837634312039887L;

	@Id
	@Column(name = "img_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "img_sequence")
	private Long id;

	@Column(name = "img_type")
	private String type;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "img_content")
	private byte[] fileImage;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "children")
	private Product product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public byte[] getFileImage() {
		return fileImage;
	}

	public void setFileImage(byte[] fileImage) {
		this.fileImage = fileImage;
	}

}